import React from 'react';
import { useFirebase } from '../Hooks/firebase';
import { Button } from '../components/button';
import { Navigation } from './navigation'

export const HomeDefault = () => {

    const { auth } = useFirebase();

    const logout = () => {
        auth.signOut();
    }

    return (
        <div className='container'>
            
            <Navigation />


        </div>
    )
}