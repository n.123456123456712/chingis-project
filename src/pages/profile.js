import React, { useEffect, useContext, useRef, useState } from 'react';
import { useDoc, useFirebase } from '../Hooks/firebase';
import { AuthContext } from '../providers/auth-user-provider';
import { Button } from '../components/button'
import { useStorage } from '../Hooks';

const Profile = () => {
    const { user } = useContext(AuthContext)
    const { uid } = user || {};
    const { data, updateRecord } = useDoc(`users/${uid}`);
    const { profileImage, username } = data || {};
    const [file, setFile] = useState('')
    const [imgSrc, setImgSrc] = useState('')
    const inputFile = useRef(null);
    const { firebase } = useFirebase();
    const profileImageSrc = useStorage(`profileImages/${uid}/profileImage.jpg`)

    useEffect(() => {
        if (inputFile) {

            function onFileChange() {
                setFile(inputFile.current.files[0]);
                setImgSrc(URL.createObjectURL(inputFile.current.files[0]))
            }

            inputFile.current.addEventListener('change', onFileChange);
        }
    }, [inputFile]);

    useEffect(() => {
        if (firebase && file && uid) {
            var storageSideRef = firebase.storage().ref().child(`profileImages/${uid}/profileImage.jpg`);;
            storageSideRef.put(file)
                .then((snapshot) => {
                    console.log('Done. Storage');
                    updateRecord({ profileImage: "img" })
                    console.log('Done. Firestore')
                })
        }
    }, [firebase, file, uid, imgSrc, updateRecord])


    return (
        <div>

            <div>
                <div style={{ backgroundColor: 'gray', backgroundSize: 'cover', backgroundImage: `url("${imgSrc === '' ? profileImageSrc : imgSrc}")` }} className='w-100 h-100 flex-center fs-50 font-comfortaa' >
                    <p className='op'>{(profileImage === "default") && (imgSrc === '') ? username[0] : ''}</p>
                </div>

                <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} />
                <Button onClick={() => { inputFile.current.click() }}>Upload Event Main Image</Button>
            </div>
        </div>
    )
}

export default Profile