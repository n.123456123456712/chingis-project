import React from 'react'
import { DropdownMenu } from '../components/';
import { useHistory } from 'react-router-dom';

export const Navigation = ({children}) => {
    let history = useHistory();

    console.log(useHistory())
    if(history) {
        if (history.location.pathname === '/profile') {
            return (
                <div>
                    <div className='pr flex items-center justify-between'>
                        <h3>Profile</h3>
                    </div>

                    {children}
                </div>
            )
        }

    }

    return (
        <div>
            <div className='pr flex items-center justify-between'>
                <h3>logo</h3>
                <DropdownMenu></DropdownMenu>
            </div>
            {children}
        </div>
    )
}