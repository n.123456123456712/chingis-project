import React, { useContext } from 'react'
import { useCol } from '../Hooks/firebase';
import { AuthContext} from '../providers/auth-user-provider';
// import { useDoc } from '../Hooks/firebase'

const moment = require('moment');

export const VoteHistory = () => {
   
    const user = {uid: 'fJzc6E35wuV55mdo8V1BzU2bkKu2'};
    const { data: votes } = useCol(`users/${user.uid}/votes`);

    return (
        <div className='flex-col'>
            <div className='flex-col br-black-3 h-350 w-300'>
                <div className='flex items-center justify-center h-175 w-294'>"Vote history"</div>
                <div className="br-black-1 h-1 w-294">
                <ul>{
                    votes && votes.map((vote) => 
                        <li> 
                            <div> Project: {vote.projectName}</div>
                            <div> Created time: {moment(vote.createdAt.toDate()).format('YYYY-MM-DD HH цаг')}</div>
                        </li>)
                }</ul>
                </div>
            </div>
        </div>
    );
}