import React, { useState } from 'react';
import { useStorage } from '../Hooks';
import { Button } from '../components';
import { useHistory } from 'react-router-dom';

export const EventRender = ({ id, name, desc, categories }) => {
    const url = useStorage(`EventImages/${id}/MainImage.jpg`);
    const history = useHistory();
    const [state, setState] = useState("Vote")

    const handleVote = () => {
        if (state === "Vote") {
            setState("Voted")
        } else {
            setState("Vote")
        }
    }

    return (
        <div className='flex-center w100'>
            <div className="mt-10 mb-10" onClick={() => { history.push(`event-id?id=${id}`)}} >
                <div className="flex-row">
                    <svg width="32" height="32" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="16.875" cy="17" r="16.5" fill="#C4C4C4" />
                    </svg>
                    <div className="font-comfortaa fs-18 lh-32 h-32 ml-10 bold">{name}</div>
                </div>
                <div className='flex items-center justify-center h-175 mt-10'
                    height="175" width="294"
                    style={{ backgroundColor: "#C0C0C0", backgroundSize: 'cover', backgroundImage: `url("${url}")` }} />
                    <div className="font-comfortaa fs-14 lh-21 mt-1">{desc}</div>
                <div className='flex-row justify-between items-center w100'>
                    <div className="rb w50 h-25 font-comfortaa fs-14 lh-25">Comments</div>
                    <Button className="rb w50 h-25" onClick={handleVote}>{state}</Button>
                </div>
            </div>
        </div>
    )
}