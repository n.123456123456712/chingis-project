import React from 'react'

export const HomeIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11.625 24.5V14.5H17.625V24.5M5.625 11.5L14.625 4.5L23.625 11.5V22.5C23.625 23.0304 23.4143 23.5391 23.0392 23.9142C22.6641 24.2893 22.1554 24.5 21.625 24.5H7.625C7.09457 24.5 6.58586 24.2893 6.21079 23.9142C5.83571 23.5391 5.625 23.0304 5.625 22.5V11.5Z" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}