import React from 'react'

export const NewProjectIcon = ({ height, width, color = "#52575C", ...others }) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10.625 6.5V14.5M6.625 10.5H14.625M20.625 10.5C20.625 16.0228 16.1478 20.5 10.625 20.5C5.10215 20.5 0.625 16.0228 0.625 10.5C0.625 4.97715 5.10215 0.5 10.625 0.5C16.1478 0.5 20.625 4.97715 20.625 10.5Z" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}