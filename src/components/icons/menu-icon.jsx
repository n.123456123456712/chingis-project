import React from 'react'

export const MenuIcon = ({height, width , color = "#333333", ...others}) => {
    return (
        <span {...others}>
            <svg width={width} height={height} viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.25 14H23.25M5.25 8H23.25M5.25 20H23.25" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
            </svg>
        </span>
    )
}