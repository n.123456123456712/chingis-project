import React, { useState } from 'react'
import { Stack, MenuIcon, ProfileIcon, NewProjectIcon, XIcon, HomeIcon } from '../components'

export const DropdownItem = ({children}) => {
    return (
        <div className='flex'>
            {children}
        </div>
    )
}

export const DropdownOptions = ({children}) => {
    return (
        <Stack className='absolute t-0 r-0 w100 b-light-gray pa-10' size={5}>
            {children}
        </Stack>
    )
}


export const DropdownMenu = () => {
    const [show, setShow] = useState(false);

    return (
        <div>
            <MenuIcon height={30} width={30} onClick={() => setShow(true)}/>
            {show && 
            <DropdownOptions>
                <XIcon className='absolute r-0 t-0' height={15} width={15} onClick={() => setShow(false)} />
                <DropdownItem>
                    <NewProjectIcon height={20} width={20} />
                    <div className='ml-10'>New project</div>
                </DropdownItem>
                <DropdownItem>
                    <ProfileIcon height={20} width={20} />
                    <div className='ml-10'>Profile</div>
                </DropdownItem>
                <DropdownItem>
                    <HomeIcon height={20} width={20} />
                    <div className='ml-10'>Home</div>
                </DropdownItem>
            </DropdownOptions>}
        </div>
    )
}