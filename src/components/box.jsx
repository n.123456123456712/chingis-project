import React from 'react';

export const Box = (props) => {
    let { children, disabled, className, ...others } = props;
   
    return (
        <div className={`box ${className}`} {...others}>
            {children}
        </div>
    );
};