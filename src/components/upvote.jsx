import React from 'react';
import { useDoc, useCol } from '../Hooks/firebase';
import { useState } from 'react';

export const Upvote = (props) => {
    let { children, disabled, className, upvoted, eventId, date, uid, ...others } = props;
    let { data: username } = useDoc(`users/${uid}`);
    let { createRecord } = useCol(`Events/${eventId}/votes`);
    const [state, setState] = useState(false);
    let tmp;
    if(username) {
        tmp = username.username;
    }
    let { deleteRecord } = useCol(`Events/${eventId}/votes/`);

    let upvote = () => {
        if(state === false) {
            setState(true);
            console.log(eventId)
            createRecord(username.username, { createdAt: date, userId: uid, username: username.username });
        } else {
            setState(false);
            deleteRecord(tmp);
        }
    }

    return (
        <button onClick={upvote} className={`btn ${className} ${disabled && 'disabled'}`} {...others}>
            {state === true ? "voted" : "vote"}
        </button>
    );
};